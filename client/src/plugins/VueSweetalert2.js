import Swal from 'sweetalert2';

class VueSweetalert2 {
  static install(vue, options) {
    const swalFunction = (...args) => {
      if (options) {
        const mixed = Swal.mixin(options);
        return mixed.fire.apply(mixed, args);
      }
      return Swal.fire.apply(Swal, args);
    };
    let methodName;
    for (methodName in Swal) {
      if (
        Object.prototype.hasOwnProperty.call(Swal, methodName) &&
        typeof Swal[methodName] === 'function'
      ) {
        swalFunction[methodName] = (method => {
          return (...args) => {
            return Swal[method].apply(Swal, args);
          };
        })(methodName);
      }
    }
    vue['swal'] = swalFunction;
    // add the instance method
    if (!vue.prototype.hasOwnProperty('$swal')) {
      vue.prototype.$swal = swalFunction;
    }
  }
}

export default VueSweetalert2;
