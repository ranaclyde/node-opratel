import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: () => import("./views/users/ListUser.vue")
    },
    {
      path: "/user",
      name: "listUser",
      component: () => import("./views/users/ListUser.vue")
    },
    {
      path: "/user/create",
      name: "createUser",
      component: () => import("./views/users/CreateUser.vue")
    },
    {
      path: "/user/:id",
      name: "editUser",
      component: () => import("./views/users/EditUser.vue")
    },
    {
      path: "/user/password/:id",
      name: "changePassword",
      component: () => import("./views/users/ChangePassword.vue")
    }
  ]
});
