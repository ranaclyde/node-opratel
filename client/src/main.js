import Vue from "vue";
import App from "./App.vue";
import router from "./router";

import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

import { ValidationObserver, ValidationProvider, localize, extend } from 'vee-validate';
import * as rules from 'vee-validate/dist/rules';
import es from 'vee-validate/dist/locale/es.json';

import VueSweetalert2 from './plugins/VueSweetalert2';

// Configuración fontawesome
library.add(fas);
Vue.component('Icon', FontAwesomeIcon);

// instalar componentes para usar globalmente
Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);

// Instalar reglas
Object.keys(rules).forEach( rule => {
  extend(rule, rules[rule]);
});

// Instalar mensajes en español
localize('es', es);

// Configuración vue-sweetalert
Vue.use(VueSweetalert2);

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
