'use strict';

const User = require('../models/userSchema');
const bcrypt = require('bcrypt');

const createData = (req, res) => {
  User.create(req.body)
    .then((data) => {
      console.log('New User Created!', data);
      res.status(201).json(data);
    })
    .catch((err) => {
      if (err.name === 'ValidationError') {
        console.error('Error Validating!', err);
        res.status(422).json(err);
      } else {
        console.error(err);
        res.status(500).json(err);
      }
    });
};

const readData = (req, res) => {
  User.find()
    .then((data) => {
      res.status(200).json(data);
    })
    .catch((err) => {
      console.error(err);
      res.status(500).json(err);
    });
};

const findData = (req, res) => {
  User.findById(req.params.id)
    .then((data) => {
      res.status(200).json(data);
    })
    .catch((err) => {
      console.error(err);
      res.status(500).json(err);
    });
};

const updateData = (req, res) => {
  console.log('User Params: ' + req.params.id);
  User.findByIdAndUpdate(req.params.id, req.body, {
    useFindAndModify: false,
    new: false,
  })
    .then((data) => {
      console.log('User updated!');
      res.status(201).json(data);
    })
    .catch((err) => {
      if (err.name === 'ValidationError') {
        console.error('Error Validating!', err);
        res.status(422).json(err);
      } else {
        console.error(err);
        res.status(500).json(err);
      }
    });
};

const deleteData = (req, res) => {
  User.findById(req.params.id)
    .then((data) => {
      if (!data) {
        throw new Error('User not available');
      }
      return data.remove();
    })
    .then((data) => {
      console.log('User removed!');
      res.status(200).json(data);
    })
    .catch((err) => {
      console.error(err);
      res.status(500).json(err);
    });
};

const updatePassword = (req, res) => {
  const password = req.body.password;
  const newPassword = req.body.newPassword;

  User.findById(req.params.id, function (err, user) {
    if (err) throw err;

    // test a matching password
    user.comparePassword(password, function (err, isMatch) {
      if (err) throw err;
      if (isMatch) {
        user.password = newPassword;
        user.save().then((data) => res.status(200).json(data));
      } else {
        res.status(400).json({ message: 'Las contraseñas no coinciden' });
      }
    });
  });
};

module.exports = {
  createData,
  readData,
  findData,
  updateData,
  updatePassword,
  deleteData,
};
