const express = require('express');

const {
  createData,
  readData,
  findData,
  updateData,
  deleteData,
  updatePassword,
} = require('../controllers/userController');

const router = express.Router();

router
  .post('/', createData)
  .get('/', readData)
  .get('/:id', findData)
  .put('/:id', updateData)
  .put('/password/:id', updatePassword)
  .delete('/:id', deleteData);

module.exports = router;
